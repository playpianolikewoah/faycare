#set the path
import os, sys
from flask.ext.script import Manager, Server
import faycare.migrate_app
from faycare.bootstrap import PopulateDB, ResetDB
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

print os.path.abspath(os.path.join(os.path.dirname(__file__),'..'))

manager = Manager(faycare.migrate_app.app)

#Turn on debugger by default and reloader
manager.add_command("runserver", Server(
	use_debugger = True,
	use_reloader = True,
	host = '0.0.0.0')
)

manager.add_command("populate_db", faycare.bootstrap.PopulateDB())
manager.add_command("reset_db", faycare.bootstrap.ResetDB())
manager.add_command("erase_db", faycare.bootstrap.EraseDB())

if __name__ == "__main__":
	manager.run()