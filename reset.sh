#!/bin/bash

cd ~/projects/py/faycare/
python faycare/dropall.py
python migrate.py reset_db
python migrate.py populate_db
