import inspect
import string
import random
# if the site is being run from a migration or the main app
if inspect.stack()[-1][1] == 'mange.py':
	from test_server import app
elif inspect.stack()[-1][1] == '/home/gototgo/venv/bin/gunicorn':
	from serve import app
else:
	from faycare.migrate_app import app
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.sql import exists
from sqlalchemy.schema import Sequence
from sqlalchemy import func, and_, desc, or_, asc
from flask.ext.login import LoginManager
from flask.ext.security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin, login_required, current_user
from flask_mail import Mail
from flask.ext.assets import Environment, Bundle
from sqlalchemy.ext.associationproxy import association_proxy
from flask.ext.babel import Babel

import wtforms_json
from pytz import timezone
from flask.ext.babel import format_datetime,format_date
from celery.utils.log import get_task_logger
from itsdangerous import JSONWebSignatureSerializer as Serializer

from flask.ext.migrate import Migrate

from celery import Celery
import pytz
import os
# from flask_debugtoolbar import DebugToolbarExtension

wtforms_json.init()

# Email
mail = Mail(app)
db = SQLAlchemy(app)
babel = Babel(app)

# toolbar = DebugToolbarExtension(app)

utc = pytz.UTC

config = app.config
assets = Environment(app)

migrate = Migrate(app,db)

roles_users = db.Table('roles_users',
	db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
	db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

kids_users = db.Table('kids_users',
	db.Column('kid_id', db.Integer(), db.ForeignKey('kid.id')),
	db.Column('user_id',db.Integer(), db.ForeignKey('user.id')))

allergen_kid = db.Table('allergen_kid',
	db.Column('allergen_id', db.Integer(), db.ForeignKey('allergen.id')),
	db.Column('kid_id', db.Integer(), db.ForeignKey('kid.id')))

orders_order_tags = db.Table('orders_order_tags',
	db.Column('order_id', db.Integer(), db.ForeignKey('order.id')),
	db.Column('order_tag_id',db.Integer(), db.ForeignKey('order_tag.id')))

class Role(db.Model, RoleMixin):
	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(80), unique=True)
	private = db.Column(db.Boolean)
	description = db.Column(db.String(255))

	def __repr__(self):
		return '<Role %r>' % (self.name)
count = 0

class User(db.Model,UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(255), unique=True)
	password = db.Column(db.String(255))
	active = db.Column(db.Boolean())
	pending = db.Column(db.Boolean())
	confirmed_at = db.Column(db.DateTime())

	last_login_at = db.Column(db.DateTime())
	current_login_at = db.Column(db.DateTime())
	last_login_ip = db.Column(db.String())
	current_login_ip = db.Column(db.String())
	login_count = db.Column(db.Integer())
	stripe_code = db.Column(db.String())

	bucket = db.Column(db.String())

	timezone = db.Column(db.String())

	status = db.Column(db.Integer())
	status_time = db.Column(db.DateTime(timezone=True))

	code = db.Column(db.String())

	person_id = db.Column(db.ForeignKey('person.id'))

	person = db.relationship('Person',
		backref=db.backref('users', lazy='dynamic'))

	first_name = association_proxy('person', 'first_name')
	middle_name = association_proxy('person', 'middle_name')
	last_name = association_proxy('person', 'last_name')
	cell_phone = association_proxy('person', 'cell_phone')

	def get_mobile_access_token(self):
		s = Serializer(app.config['SECRET_KEY'])
		return s.dumps({'id':self.id,'email':self.email})


	roles = db.relationship('Role', secondary=roles_users,
							backref=db.backref('users', lazy='dynamic'))
	@property
	def active_kids(self):
		return self.kids.filter(Kid.hold == False)

	def is_teacher(self):
		return user_datastore.find_role('teacher') in self.roles

	def is_le_admin(self):
		return user_datastore.find_role('admin') in self.roles

	def is_director(self):
		return user_datastore.find_role('director') in self.roles

	def is_parent(self):
		return user_datastore.find_role('parent') in self.roles

	def is_attendance(self):
		return user_datastore.find_role('attendance') in self.roles

	def has_role(self,role):
		return user_datastore.find_role(role) in self.roles

	def is_active(self):
		return self.active

	def __repr__(self):
		return '<User %r>' % (self.email)

schools_users = db.Table('schools_users',
	db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
	db.Column('school_id',db.Integer(), db.ForeignKey('school.id')))

groups_kids = db.Table('groups_kids',
	db.Column('group_id', db.Integer(), db.ForeignKey('group.id')),
	db.Column('kid_id',db.Integer(), db.ForeignKey('kid.id')))

groups_users = db.Table('groups_users',
	db.Column('group_id', db.Integer(), db.ForeignKey('group.id')),
	db.Column('user_id', db.Integer(), db.ForeignKey('user.id')))

kids_photos = db.Table('kids_photos',
	db.Column('kid_id', db.Integer(), db.ForeignKey('kid.id')),
	db.Column('photo_id', db.Integer(), db.ForeignKey('activity_photo.id')))

class HistoryItem(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	created_at = db.Column(db.DateTime())
	description = db.Column(db.String())
	title = db.Column(db.String())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('history_items', lazy='dynamic'))
	kid_name = db.Column(db.String())
	staff_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	staff = db.relationship('User',
		backref=db.backref('history_items', lazy='dynamic'))
	staff_name = db.Column(db.String())
	related_to = db.Column(db.String())
	previous_value = db.Column(db.String())
	new_value = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('history_items', lazy='dynamic'))

class Plan(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	limit = db.Column(db.Integer())
	price = db.Column(db.Integer())
	overage = db.Column(db.Float())

class CreditCard(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	uuid = db.Column(db.String())
	last4 = db.Column(db.String())
	exp_month = db.Column(db.String(2))
	exp_year = db.Column(db.String(4))
	type = db.Column(db.String())
	customer_id = db.Column(db.ForeignKey('customer.id'))
	customer = db.relationship('Customer',
		backref=db.backref('credit_cards', lazy='dynamic'))

class Subscription(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	uuid = db.Column(db.String())
	start = db.Column(db.DateTime())
	current_period_end = db.Column(db.DateTime())
	current_period_start = db.Column(db.DateTime())
	customer_id = db.Column(db.ForeignKey('customer.id'))
	customer = db.relationship('Customer',
		backref=db.backref('subscription', uselist=False))

class WeeklyHour(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	start_time = db.Column(db.DateTime())
	end_time = db.Column(db.DateTime())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('weekly_hours', lazy='dynamic'))
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('weekly_hours', lazy='dynamic'))

class Coupon(db.Model):
	id = db.Column(db.Integer, primary_key=True)

class Customer(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	uuid = db.Column(db.String())
	discount = db.Column(db.String())
	user_id = db.Column(db.ForeignKey('user.id'))
	user = db.relationship('User',
		backref=db.backref('customer', uselist=False))

class AgeRange(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('age_ranges', lazy='dynamic'))
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('age_ranges', lazy='dynamic'))
	start_age = db.Column(db.Integer())
	end_age = db.Column(db.Integer())
	needs_potty_trained = db.Column(db.Boolean())

class ActivityPhoto(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	pic_small = db.Column(db.String())
	pic_smallish = db.Column(db.String())
	pic_med = db.Column(db.String())
	pic_large = db.Column(db.String())
	time = db.Column(db.DateTime())

	kids = db.relationship('Kid', secondary=kids_photos,
		lazy='dynamic',
		backref=db.backref('photos', lazy='dynamic'))

	title = db.Column(db.String())
	description = db.Column(db.String())


class Person(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.String(100))
	middle_name = db.Column(db.String(100))
	last_name = db.Column(db.String(100))

	home_phone = db.Column(db.String(20))
	cell_phone = db.Column(db.String(20))
	work_phone = db.Column(db.String(20))
	street = db.Column(db.String())
	city = db.Column(db.String())
	state = db.Column(db.String())
	zip_code = db.Column(db.String())

	@property
	def full_name(self):
		return self.first_name + ' ' + self.last_name

	def __repr__(self):
		return '<Person %r %r>' % (self.first_name,self.last_name)

messages_users = db.Table('messages_users',
	db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
	db.Column('message_id', db.Integer(), db.ForeignKey('message.id')))

class Message(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	sender = db.relationship('User',
		backref=db.backref('messages', uselist=True))
	recipients = db.relationship('User', secondary='messages_users',
		backref=db.backref('recipients', lazy='dynamic'))
	time = db.Column(db.DateTime())
	message = db.Column(db.String())
	status = db.Column(db.Integer())

class Allergen(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('allergens', lazy='dynamic'))
	kids = db.relationship('Kid', secondary=allergen_kid,
		lazy='dynamic',
		backref=db.backref('allergens', lazy='dynamic'))

class Vaccination(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	vaccine = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('vaccinations', lazy='dynamic'))

	@property
	def due_dates_list(self):
		return ','.join(['Due at ' + str(due_date.months) + ' months' for due_date in self.due_dates])

class DueDate(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	months = db.Column(db.Integer())
	vaccination_id = db.Column(db.Integer(), db.ForeignKey('vaccination.id'))
	vaccination = db.relationship('Vaccination', cascade='delete-orphan, delete', single_parent=True,
		backref=db.backref('due_dates', lazy='dynamic'))

class Signature(db.Model):

	__tablename__ = 'signatures'
	sign_form_id = db.Column(db.Integer, db.ForeignKey('sign_form.id'), primary_key=True)
	kid_id = db.Column(db.Integer, db.ForeignKey('kid.id'), primary_key=True)
	signature = db.Column(db.String())
	signed = db.Column(db.Boolean())
	extra_data = db.Column(db.String())

	def __init__(self,kid,signature=None):
		self.kid = kid
		self.signature = signature

	@property
	def serialize(self):
		return {
			'signed': self.signed,
			'kid' : self.kid.serialize,
			'extra_data':self.extra_data
		}

	kid = db.relationship('Kid',lazy='joined')

class SignForm(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String())
	content = db.Column(db.String())
	markdown = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('sign_forms', lazy='dynamic'))
	signatures = db.relationship("Signature", cascade='all, delete-orphan',
		backref='sign_form')
	active = db.Column(db.Boolean())
	requires_signature = db.Column(db.Boolean())
	kids = association_proxy("signatures", "kids")

	@property
	def kids(self):
		return [s.kid for s in self.signatures]

	@kids.setter
	def kids(self, value):
		'setting'
		self.signatures = []
		for k in value:
			self.signatures.append(Signature(k))



class Company(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	stripe_publishable_key = db.Column(db.String())
	access_token = db.Column(db.String())
	stripe_user_id = db.Column(db.String())
	stripe_code = db.Column(db.String())
	plan_id = db.Column(db.Integer(), db.ForeignKey('plan.id'))
	plan = db.relationship('Plan',
		backref=db.backref('schools', lazy='dynamic'))
	admin_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	admin = db.relationship('User',
		backref=db.backref('company', uselist=False))

	@property
	def total_kids(self):
		total = 0
		for school in self.schools:
			total = total + school.kids.count()
		return total

class School(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(255))
	u_name = db.Column(db.String(255), unique=True)
	city = db.Column(db.String(255))
	state = db.Column(db.String(2))
	zip_code = db.Column(db.String(15))
	phone_number = db.Column(db.String(20))
	ext = db.Column(db.String(10))
	grace = db.Column(db.Integer(),default=-1)
	description = db.Column(db.String())
	website_on = db.Column(db.Boolean())
	timezone = db.Column(db.String())
	testing = db.Column(db.Boolean(),default=True)
	searchable = db.Column(db.Boolean())
	website = db.Column(db.String())
	bill_equal = db.Column(db.Boolean())
	billing_on = db.Column(db.Boolean(),default=True)
	cycle_start = db.Column(db.Integer(),default=1)
	company_id = db.Column(db.Integer(), db.ForeignKey('company.id'))
	company = db.relationship('Company',
		backref=db.backref('schools', lazy='dynamic'))

	def __repr__(self):
		return '<School %r>' % (self.name)

	users = db.relationship('User', secondary=schools_users,
							backref=db.backref('schools', lazy='dynamic'))

	@property
	def serialize_kids(self):
		json_kids = []
		for kid in self.kids:
			json_kids.append(kid.serialize)
		return json_kids

class Activity(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	activity_type_id = db.Column(db.Integer(), db.ForeignKey('activity_type.id'))
	activity_type = db.relationship('ActivityType',
		backref=db.backref('activities', lazy='dynamic'))
	time = db.Column(db.DateTime(timezone=True))
	description = db.Column(db.String())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('activities', lazy='dynamic'))
	staff_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	staff = db.relationship('User',
		backref=db.backref('activities', lazy='dynamic'))
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('activities', lazy='dynamic'))

	@property
	def serialize(self):
		short_time = format_datetime(self.time,'short')
		return {'id':self.id,
			'name':self.activity_type.name,
			'time':short_time,
			'staff':self.staff.person.full_name,
			'description':self.description}

class ActivityType(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = 	db.Column(db.String)
	description = db.Column(db.String)
	icon = db.Column(db.String())

	@property
	def serialize(self):
		return {'name':self.name,'id':self.id}

	def __repr__(self):
		return '<ActivityType %r>' % self.name

class Kid(db.Model):
	__tablename__ = 'kid'
	__searchable__ = ['first_name','last_name','current_group']

	id = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.Unicode(255))
	middle_name = db.Column(db.String(255))
	last_name = db.Column(db.Unicode(255))
	dob = db.Column(db.DateTime(timezone=True))
	profile_pic_small = db.Column(db.String())
	profile_pic_smallish = db.Column(db.String())
	profile_pic_med = db.Column(db.String())
	profile_pic_large = db.Column(db.String())
	gender = db.Column(db.String(1))
	current_group = db.Column(db.String())
	status = db.Column(db.Integer(1))
	status_time = db.Column(db.DateTime(timezone=True))
	potty_trained = db.Column(db.Boolean())
	pending = db.Column(db.Boolean())
	scholarship = db.Column(db.Float(),default=0)
	govt = db.Column(db.Float(),default=0)
	extra_credit = db.Column(db.Float(),default=0)

	hold = db.Column(db.Boolean(),default=False)

	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('kids', lazy='dynamic'))

	between_schedule_id = db.Column(db.Integer(), db.ForeignKey('between_schedule.id'))
	between_schedule = db.relationship('BetweenSchedule',
		backref=db.backref('kids', lazy='dynamic'))

	one_for_all_schedule_id = db.Column(db.Integer(), db.ForeignKey('one_for_all_schedule.id'))
	one_for_all_schedule = db.relationship('OneForAllSchedule',
		backref=db.backref('kids', lazy='dynamic'))

	hourly_schedule_id = db.Column(db.Integer(), db.ForeignKey('hourly_schedule.id'))
	hourly_schedule = db.relationship('HourlySchedule',
		backref=db.backref('kids', lazy='dynamic'))

	calendar_schedule_id = db.Column(db.Integer(), db.ForeignKey('calendar_schedule.id'))
	calendar_schedule = db.relationship('CalendarSchedule',
		backref=db.backref('kids', lazy='dynamic'))

	code = db.Column(db.Integer(unsigned=True,zerofill=True),
		Sequence('kid_code_seq',start=1001, increment=1),
		primary_key=False)

	parents = db.relationship('User', secondary=kids_users,
							backref=db.backref('kids', lazy='dynamic'))

	# schools = db.relationship('School', secondary=kids_schools,
	# 						backref=db.backref('kids', lazy='dynamic'))

	@property
	def serialize(self):
		return {'id':self.id,
		'first_name':self.first_name,
		'last_name':self.last_name,
		'full_name':"%s %s" % (self.first_name,self.last_name),
		'pic': self.profile_pic_small,
		'group' : self.groups[0].name,
		'med_pic' : self.profile_pic_med,
		'status' : self.status,
		'large_pic' : self.profile_pic_large,
		'status':self.status,
		'status_time':format_datetime(self.status_time, 'short')
		}

	@property
	def sign_forms(self):
		q = db.session.query(SignForm).join('signatures','kid')
		q = q.filter(Kid.id == self.id)
		return [sign for sign in q]

	@property
	def full_name(self):
		return '%s %s' % (self.first_name, self.last_name)

	@property
	def last_activity(self):
		return self.activities.filter().order_by(Activity.id.desc()).first()

	@property
	def display_time(self):
		return format_datetime(self.status_time, 'short')

	@property
	def display_short_time(self):
		return format_datetime(self.status_time, 'h:mm')

	@property
	def smallish_profile_pic(self):
		if self.profile_pic_smallish:
			return '<img src="%s" alt="self.full_name" />' % self.profile_pic_smallish
		else:
			return '<img src="%s" alt="self.full_name" />' % 'http://placekitten.com/130/130'

	def __repr__(self):
		return '<Kid %r %r>' % (self.first_name,self.last_name)

class Attendance(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime(timezone=True))
	status = db.Column(db.Integer(1))
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('attendances', lazy='dynamic'))
	user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	user = db.relationship('User',
		backref=db.backref('attendances', lazy='dynamic'))
	attendance_type = db.Column(db.Integer())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('attendances', lazy='dynamic'))

	@property
	def serialize(self):
		short_time = format_datetime(self.time, 'short')
		return {'id':self.id,
				'time':short_time,
				'user':self.user.person.full_name,
				'status':self.status}

class UserImportStatus(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	user = db.relationship('User',
		backref=db.backref('import_status', uselist=False))
	status = db.Column(db.Integer())

class TempFile(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime())
	path = db.Column(db.String())

class Group(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(255))
	min_age = db.Column(db.Integer())
	max_age = db.Column(db.Integer())
	needs_potty_trained = db.Column(db.Boolean())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('groups', lazy='dynamic'))

	kids = db.relationship('Kid', secondary=groups_kids,
		lazy='dynamic',
		backref=db.backref('groups', lazy='dynamic'))

	staff = db.relationship('User', secondary=groups_users,
		backref=db.backref('groups', lazy='dynamic'))

	@property
	def active_kids(self):
		return self.kids.filter(Kid.hold == False).order_by(Kid.last_name)

	@property
	def serialize(self):
		kids = []
		for kid in self.kids:
			kids.append(kid.serialize)

		return {
			'id':self.id,
			'name':self.name,
			'kids':kids
		}

	@property
	def kids_sorted(self):
		return self.kids.filter(Kid.hold == False).order_by(desc(Kid.status),Kid.last_name)

	@property
	def kids_sorted_hold(self):
		return self.kids.filter(Kid.hold == True).order_by(Kid.last_name)		
    
	@property
	def kids_in_count(self):
		return self.kids.filter(Kid.status == 1).count()

	@property
	def kids_in(self):
		return self.kids.filter(Kid.status == 1).all()

	@property
	def kids_out(self):
		return self.kids.filter(Kid.status == 1).all()

	def __repr__(self):
		return '<Group %r>' % (self.name)

betweens_prices = db.Table('betweens_prices',
	db.Column('between_id', db.Integer(), db.ForeignKey('between_schedule.id')),
	db.Column('price_id', db.Integer(), db.ForeignKey('price.id')))

ones_prices = db.Table('ones_prices',
	db.Column('one_id', db.Integer(), db.ForeignKey('one_for_all_schedule.id')),
	db.Column('price_id', db.Integer(), db.ForeignKey('price.id')))

hourlies_prices = db.Table('hourlies_users',
	db.Column('hourly_id', db.Integer(), db.ForeignKey('hourly_schedule.id')),
	db.Column('price_id', db.Integer(), db.ForeignKey('price.id')))

calendars_prices = db.Table('calendars_prices',
	db.Column('calendar_id', db.Integer(), db.ForeignKey('calendar_schedule.id')),
	db.Column('price_id', db.Integer(), db.ForeignKey('price.id')))

class Price(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	price = db.Column(db.Float(),default=0.0)
	late_price = db.Column(db.Float(),default=0.0)
	group_id = db.Column(db.Integer(), db.ForeignKey('group.id'))
	group = db.relationship('Group',
		backref=db.backref('prices', lazy='dynamic'))

	age_range_id = db.Column(db.Integer(), db.ForeignKey('age_range.id'))
	age_range = db.relationship('AgeRange',
		backref=db.backref('prices', lazy='dynamic'))
	# in days
	billing_duration = db.Column(db.Integer())
	billing_unit = db.Column(db.Integer())

class Log(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	category = db.Column(db.String())
	time = db.Column(db.DateTime())
	what = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('logs', lazy="dynamic"))

class Bill(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime())
	month = db.Column(db.Integer())
	year = db.Column(db.Integer())
	amount = db.Column(db.Float())
	per_day = db.Column(db.Float())
	notified = db.Column(db.Boolean,default=False)
	days_per_week = db.Column(db.String())
	description = db.Column(db.String())
	is_calendar = db.Column(db.Boolean())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	status = db.Column(db.Integer(),default=0)
	active = db.Column(db.Boolean(),default=False)
	school = db.relationship('School',
		backref=db.backref('bills', lazy='dynamic'))
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('bills', lazy='dynamic'))
	invoice_id = db.Column(db.Integer(), db.ForeignKey('invoice.id'))
	invoice = db.relationship('Invoice',
		backref=db.backref('bills', lazy='dynamic'))

class Credit(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime())
	amount = db.Column(db.Float())
	description = db.Column(db.String())
	month = db.Column(db.Integer())
	year = db.Column(db.Integer())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	status = db.Column(db.Integer(),default=0)
	credit_type_id = db.Column(db.Integer(), db.ForeignKey('credit_type.id'))
	credit_type = db.relationship('CreditType',
		backref=db.backref('credit', uselist=True))
	school = db.relationship('School',
		backref=db.backref('credits', lazy='dynamic'))
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('credits', lazy='dynamic'))
	bill_id = db.Column(db.Integer(), db.ForeignKey('bill.id'))
	bill = db.relationship('Bill',
		backref=db.backref('credits', lazy='dynamic'))
	invoice_id = db.Column(db.Integer(), db.ForeignKey('invoice.id'))
	invoice = db.relationship('Invoice',
		backref=db.backref('credits', lazy='dynamic'))

class CreditType(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())

class Fee(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime())
	reason = db.Column(db.String())
	amount = db.Column(db.Float())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('fees', lazy='dynamic'))
	invoice_id = db.Column(db.Integer(), db.ForeignKey('invoice.id'))
	invoice = db.relationship('Invoice',
		backref=db.backref('fees', lazy='dynamic'))

class Invoice(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time = db.Column(db.DateTime())
	month = db.Column(db.Integer())
	year = db.Column(db.Integer())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('invoices', lazy='dynamic'))
	paid = db.Column(db.Float(),default=0)
	paid_off = db.Column(db.Boolean(),default=False)
	charge_id = db.Column(db.Integer(), db.ForeignKey('charge.id'))
	charge = db.relationship('Charge',
		backref=db.backref('invoices', lazy='dynamic'))

	@property
	def total(self):
		money = 0
		for bill in self.bills:
			money = money + bill.amount
		return money

	@property
	def remaining(self):
		money = 0
		for bill in self.bills:
			try:
				money = float(money) + float(bill.amount)
			except TypeError:
				print 'HEY'
				print money
				print bill.amount

		for credit in self.credits:
			try:
				money = float(money) - float(credit.amount)
			except TypeError:
				print 'HEY'
				print money
				print credit.amount
		return money

class Charge(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	amount = db.Column(db.Float())
	time = db.Column(db.DateTime())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('charges', lazy='dynamic'))

class Event(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	time_start = db.Column(db.DateTime())
	time_end = db.Column(db.DateTime())
	title = db.Column(db.String())
	content = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('events', uselist=False))

class SchoolHours(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	mon_start = db.Column(db.Time())
	closed_mon = db.Column(db.Boolean())
	mon_end = db.Column(db.Time())
	tues_start = db.Column(db.Time())
	closed_tues = db.Column(db.Boolean())
	tues_end = db.Column(db.Time())
	weds_start = db.Column(db.Time())
	closed_weds = db.Column(db.Boolean())
	weds_end = db.Column(db.Time())
	thurs_start = db.Column(db.Time())
	closed_thurs = db.Column(db.Boolean())
	thurs_end = db.Column(db.Time())
	fri_start = db.Column(db.Time())
	closed_fri = db.Column(db.Boolean())
	fri_end = db.Column(db.Time())
	sat_start = db.Column(db.Time())
	closed_sat = db.Column(db.Boolean())
	sat_end = db.Column(db.Time())
	sun_start = db.Column(db.Time())
	closed_sun = db.Column(db.Boolean())
	sun_end = db.Column(db.Time())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('school_hours', uselist=False))

class BetweenSchedule(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	mon_start = db.Column(db.Time())
	mon_end = db.Column(db.Time())
	closed_mon = db.Column(db.Boolean())
	tues_start = db.Column(db.Time())
	tues_end = db.Column(db.Time())
	closed_tues = db.Column(db.Boolean())
	weds_start = db.Column(db.Time())
	weds_end = db.Column(db.Time())
	closed_weds = db.Column(db.Boolean())
	thurs_start = db.Column(db.Time())
	thurs_end = db.Column(db.Time())
	closed_thurs = db.Column(db.Boolean())
	fri_start = db.Column(db.Time())
	fri_end = db.Column(db.Time())
	closed_fri = db.Column(db.Boolean())
	sat_start = db.Column(db.Time())
	sat_end = db.Column(db.Time())
	closed_sat = db.Column(db.Boolean())
	sun_start = db.Column(db.Time())
	sun_end = db.Column(db.Time())
	closed_sun = db.Column(db.Boolean())
	active = db.Column(db.Boolean(),default=False)
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('between_schedules', lazy='dynamic'))

	prices = db.relationship('Price', secondary=betweens_prices,
		lazy='dynamic',
		backref=db.backref('between_schedules', lazy='dynamic'))

	def __repr__(self):
		return '<BetweenSchedule %r>' % (self.name)

class OneForAllSchedule(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	active = db.Column(db.Boolean(),default=False)
	school = db.relationship('School',
		backref=db.backref('one_for_all_schedule', uselist=False))

	prices = db.relationship('Price', secondary=ones_prices,
		lazy='dynamic',
		backref=db.backref('one_for_all_schedules', lazy='dynamic'))

class HourlySchedule(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	active = db.Column(db.Boolean(),default=False)
	school = db.relationship('School',
		backref=db.backref('hourly_schedule', uselist=False))

	prices = db.relationship('Price', secondary=hourlies_prices,
		lazy='dynamic',
		backref=db.backref('hourly_schedules', lazy='dynamic'))

class CalendarSchedule(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	active = db.Column(db.Boolean(),default=False)
	school = db.relationship('School',
		backref=db.backref('calendar_schedule', uselist=False))

	prices = db.relationship('Price', secondary=calendars_prices,
		lazy='dynamic',
		backref=db.backref('calendar_schedules', lazy='dynamic'))

class CalendarAttendanceDay(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	date = db.Column(db.DateTime())
	kid_id = db.Column(db.Integer(), db.ForeignKey('kid.id'))
	kid = db.relationship('Kid',
		backref=db.backref('calendar_attendance_days', lazy='dynamic'))
	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('calendar_attendance_days', lazy='dynamic'))
	approved = db.Column(db.Boolean,default=False)

class Rejection(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(255))
	reason = db.Column(db.String())
	time = db.Column(db.DateTime())

class Invite(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(255), unique=True)
	school = db.Column(db.String(255))
	special_code = db.Column(db.String(20))
	approved = db.Column(db.Boolean())

class Order(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	content = db.Column(db.String(255))
	created_at = db.Column(db.DateTime())
	updated_at = db.Column(db.DateTime())
	due_date = db.Column(db.DateTime())
	status = db.Column(db.Integer())

	order_type_id = db.Column(db.Integer(), db.ForeignKey('order_type.id'))
	order_type = db.relationship('OrderType',
		backref=db.backref('orders',lazy='dynamic'))

	school_id = db.Column(db.Integer(), db.ForeignKey('school.id'))
	school = db.relationship('School',
		backref=db.backref('orders',lazy='dynamic'))
	assignee_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	assignee = db.relationship('User',
		backref=db.backref('orders',lazy='dynamic'))
	priority_id = db.Column(db.Integer(), db.ForeignKey('order_priority.id'))
	priority = db.relationship('OrderPriority',
		backref=db.backref('orders',lazy='dynamic'))

	@property
	def display_time(self):
		return format_date(self.due_date)

	@property
	def display_created_at(self):
		return format_date(self.created_at)

	@property
	def display_updated_at(self):
		return format_date(self.updated_at)

class OrderType(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())

class OrderPriority(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String())

class OrderComment(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.String(255))
	created_at = db.Column(db.DateTime())
	updated_at = db.Column(db.DateTime())
	author_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	author = db.relationship('User',
		backref=db.backref('order_comments',lazy='dynamic'))
	order_id = db.Column(db.Integer(), db.ForeignKey('order.id'))
	order = db.relationship('Order',
		backref=db.backref('order_comments',lazy='dynamic'))

	@property
	def serialize(self):
		return {
			'author':self.author.person.full_name,
			'created_at':format_datetime(self.created_at),
			'content':self.content
		}

	@property
	def display_created_at(self):
		return format_datetime(self.created_at,'short')

class OrderTag(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))

	order = db.relationship('Order', secondary='orders_order_tags',
		backref=db.backref('order_tags',lazy='dynamic'))

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)




@babel.timezoneselector
def get_timezone():
	if not current_user.is_anonymous():
		if current_user.timezone:
			return timezone(current_user.timezone)
		else: 
			return timezone('UTC')

def sample_string(size=5):
	return ''.join(random.sample(string.letters*size,size))

def get_u_name():
	def get_unique():
		try_string = sample_string()
		if db.session.query(exists().where(School.u_name == try_string)).scalar():
			return get_unique()
		else:
			return try_string

	return get_unique()

def get_special_code():
	def get_unique():
		try_string = sample_string()
		if db.session.query(exists().where(Invite.special_code == try_string)).scalar():
			return get_unique()
		else:
			return try_string

	return get_unique()

def make_celery(app):
	celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
	celery.conf.update(app.config)
	TaskBase = celery.Task
	class ContextTask(TaskBase):
		abstract = True
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return TaskBase.__call__(self, *args, **kwargs)
	celery.Task = ContextTask
	return celery

celery = make_celery(app)

logger = get_task_logger(__name__)
