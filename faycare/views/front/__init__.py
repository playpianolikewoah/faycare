from flask import Blueprint, render_template, request, flash, redirect, jsonify,g, url_for, after_this_request
from flask.views import MethodView
from flask_wtf import Form
from sqlalchemy import func
from faycare.models import db,Invite
import inspect
if inspect.stack()[-1][1] == 'mange.py':
	from test_server import app
elif inspect.stack()[-1][1] == '/home/gototgo/venv/bin/gunicorn':
	from serve import app
else:
	from faycare.migrate_app import app
from flask.ext.security import current_user, login_required, roles_required
from flask.ext.security.forms import ForgotPasswordForm
from wtforms import TextField, PasswordField, validators, \
    SubmitField, HiddenField, BooleanField, ValidationError, Field, \
    RadioField

import faycare.apikeys
import faycare.models
from flask.ext.security.utils import encrypt_password
import datetime 

front = Blueprint('front', __name__,template_folder='templates')
def before_front():
	if request.endpoint != 'front.home'\
		and request.endpoint != 'front.tour'\
		and request.endpoint != 'front.forgot_password'\
		and request.endpoint != 'front.reset_password'\
		and request.endpoint != 'front.reset_password_plain'\
		and request.endpoint != 'front.invite':
		if not current_user.is_authenticated():
			return redirect('/register/')
		else:
			g.is_admin = current_user.is_le_admin()
			

front.before_request(before_front)

taglines = [
			'Simply manage your childcare.<br >Less time on computers, more time watching kids.',
			'Childcare management for people who still love paper.<br/>We\'ll save the environment and your time.',
			'You get more time with your kids <br />and less money out of your budget.'
			]

class InviteView(MethodView):
	def get(self):
		import random
		if not current_user.is_authenticated():
			return render_template('front/invite.html',invite_form=InviteForm(),tagline=random.choice(taglines))
		else:
			if current_user.schools.all():
				return redirect('/%s/' % current_user.schools.first().u_name)
			else:
				return redirect('/setup/')

	def post(self):
		import mandrill
		form = InviteForm(request.form)
		if form.validate():
			special_code = faycare.models.get_special_code()
			invite = Invite(email=form.email.data,school=form.school.data,special_code=special_code,
				approved=False)
			db.session.add(invite)
			db.session.commit()
			
			template = render_template('front/email/invite.html',
				email=form.email.data,
				school=form.school.data,
				special_code=special_code)

			message_obj = {
				'html':template,
				'subject':'[GoTotGo] Invitation Request',
				'from_email':'jacob@gototgo.com',
				'to':[{
						'email':'jacob@gototgo.com',
						'name': 'Jacob Schatz'
					}],
				'view_content_link':False,
				'tags':['invitation request']
			}
			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)
			m.messages.send(message_obj)
			flash('Thanks for requesting an invite. We\'ll contact you when your invitation is ready. Email jacob@gototgo.com with any questions.')
			return render_template('front/invite.html',invite_form=InviteForm())
		else:
			return render_template('front/invite.html',invite_form=form)

class HomeView(MethodView):
	def get(self):
		import random
		if not current_user.is_authenticated():
			return render_template('front/index.html',tagline=random.choice(taglines))
		else:
			if current_user.schools.all():
				return redirect('/%s/' % current_user.schools.first().u_name)
			else:
				return redirect('/setup/')

class TourView(MethodView):
	def get(self):
		return render_template('front/tour.html')

class SetupRoleView(MethodView):
	def get(self):
		if len(current_user.roles):
			return redirect('/setup/2/')
		else:
			setup_user_form = SetupUserForm()
			return render_template('front/setup.html',setup_user_form=setup_user_form)

	def post(self):
		setup_user_form = SetupUserForm(request.form)
		if setup_user_form.validate():
			chosen_role = str(setup_user_form.data['roles'])
			role = faycare.models.Role.query.filter_by(name=chosen_role).first()
			faycare.models.user_datastore.add_role_to_user(current_user,role)
			person = faycare.models.Person()
			current_user.person = person
			current_user.timezone = setup_user_form.timezone.data
			faycare.models.db.session.commit()
			faycare.models.user_datastore.commit()
			return redirect('/setup/2/')
		return render_template('front/setup.html',setup_user_form=setup_user_form)

class SetupUserForm(Form):
	roles = RadioField(choices=[(r.name,r.name.title()) for r in faycare.models.Role.query.filter_by(private=False)],validators=[validators.Required()])
	timezone = HiddenField()

class SetupSchoolView(MethodView):
	def get(self):		
		if not len(current_user.roles):
			return redirect('/setup/')
		elif len(current_user.schools.all()):
			return redirect('/%s/' % current_user.schools.first().u_name)
		else:
			setup_school_form = SetupSchoolForm(new_existing='new',school_choice='none')
			return render_template('front/setup_school.html',setup_school_form=setup_school_form)

	def post(self):
		if 'school_choice' in request.form:
			choose_school_form = ChooseSchoolForm(request.form)
			if not current_user.schools.all():
				chosen_school = faycare.models.School.query.get(choose_school_form.school_choice.data)
				if chosen_school:
					current_user.schools.append(chosen_school)
					current_user.pending = True
					faycare.models.db.session.commit()
					return redirect('/%s/' % current_user.schools.first().u_name)
				else:
					return redirect('/setup/2/')
			else:
				return redirect('/setup/2/')
		else:
			setup_school_form = SetupSchoolForm(request.form)
			if setup_school_form.validate():
				if current_user.is_teacher() or current_user.is_parent():
					school_suggestions = faycare.models.School.query.filter(
						func.lower(faycare.models.School.name) == func.lower(setup_school_form.name.data),
						func.lower(faycare.models.School.city) == func.lower(setup_school_form.city.data),
						func.lower(faycare.models.School.state) == func.lower(setup_school_form.state.data),
						faycare.models.School.searchable == True
					).all()
					suggestions = True
					if len(school_suggestions) == 0:
						suggestions = False
						choose_school_form = None
					else:
						choose_school_form = ChooseSchoolForm()
						choose_school_form.school_choice.choices = [(suggestion.id, "%s in %s, %s" % (suggestion.name.title(), suggestion.city.title(), suggestion.state.upper())) for suggestion in school_suggestions]
					return render_template('front/setup_school.html',
						setup_school_form=setup_school_form,
						choose_school_form=choose_school_form,
						school_suggestions=school_suggestions,
						suggestions=str(suggestions))
				elif g.is_admin:
					new_company = faycare.models.Company(plan=faycare.models.Plan.query.filter_by(name='free').first(),admin=current_user)
					faycare.models.db.session.add(new_company)
					new_school = faycare.models.School(
						name=setup_school_form.name.data,
						city=setup_school_form.city.data,
						company=new_company,
						state=setup_school_form.state.data,
						u_name =faycare.models.get_u_name(),
						timezone=current_user.timezone,
						searchable=True,
					)
					days = ['mon','tues','weds','thurs','fri','sat','sun']
					school_hours = faycare.models.SchoolHours()
					for day in days:
						setattr(school_hours,'%s_start' % day,datetime.time(6,30,00))
						setattr(school_hours,'%s_end' % day,datetime.time(18,30,00))
						if day == 'sat' or day == 'sun':
							setattr(school_hours,'closed_%s' % day,True)
						else:
							setattr(school_hours,'closed_%s' % day,False)
					school_hours.name = '%s school hours' % new_school.name
					faycare.models.db.session.add(school_hours)
					school_hours.school = new_school
					attendance_user = faycare.models.user_datastore.create_user(
						email='%s@%s.com' % (new_school.u_name,faycare.apikeys.app_name),
						password=encrypt_password(faycare.models.sample_string(15)))
					faycare.models.user_datastore.add_role_to_user(
						attendance_user, 
						faycare.models.user_datastore.find_role('attendance')
					)
					attendance_user.schools.append(new_school)
					current_user.schools.append(new_school)
					faycare.models.db.session.add(new_school)
					faycare.models.db.session.commit()
					return redirect('/dash/')
		return render_template('front/setup_school.html',setup_school_form=setup_school_form)

class StripeConnectView(MethodView):
	def get(self):
		import urllib
		import urllib2
		import json

		if current_user.is_authenticated():
			if g.is_admin and 'code' in request.args:
				
				temp_token = request.args['code']
				
				url = faycare.apikeys.stripe_auth_token
				values = { 'client_secret' : faycare.apikeys.stripe_api_secret,
					'code' : temp_token,
					'grant_type' : 'authorization_code' }

				data = urllib.urlencode(values)
				req = urllib2.Request(url, data)
				response = urllib2.urlopen(req).read()
				json_response = json.loads(response)

				current_user.schools[0].company.stripe_publishable_key = json_response['stripe_publishable_key']
				current_user.schools[0].company.access_token = json_response['access_token']
				current_user.schools[0].company.stripe_user_id = json_response['stripe_user_id']
				current_user.schools[0].company.stripe_code = request.args['code']

				faycare.models.db.session.commit()
				return redirect('/' + current_user.schools[0].u_name + '/bills/')
			else:
				print 'no code or not admin'
				return redirect('/')	
		else:
			print 'not authenticated'
			return redirect('/')

def send_reset_password_instructions(user):
	from flask.ext.security.utils import md5
	import mandrill
	data = [str(user.id), md5(user.password)]
	token = faycare.models.security.reset_serializer.dumps(data)
	
	password_link = 'https://gototgo.com/reset_password/%s/' % token
	template = render_template('front/email/forgot_password.html',password_link=password_link)

	message_obj = {
		'html':template,
		'subject':'[GoTotGo] Password Reset',
		'from_email':'jacob@gototgo.com',
		'to':[{
				'email':user.email,
				'name': user.person.first_name
			}],
		'view_content_link':False,
		'tags':['password reset']
	}
	m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)
	m.messages.send(message_obj)
	flash('We sent an email to that address with instructions on resetting your password.')
	return redirect(url_for('front.forgot_password')) 

class ForgotPasswordView(MethodView):
	def get(self):
		return render_template('front/forgot_password.html',forgot_password_form=ForgotPasswordForm())

	def post(self):
		form = ForgotPasswordForm(request.form)
		if form.validate_on_submit():
			return send_reset_password_instructions(form.user)
		else:
			return render_template('front/forgot_password.html',forgot_password_form=form)

def get_token_status(token, serializer, max_age=None):
	from itsdangerous import BadSignature, SignatureExpired
	from flask.ext.security.utils import get_max_age 

	serializer = getattr(faycare.models.security, serializer + '_serializer')
	max_age = get_max_age(max_age)
	user, data = None, None
	expired, invalid = False, False

	try:
		data = serializer.loads(token, max_age=max_age)
	except SignatureExpired:
		d, data = serializer.loads_unsafe(token)
		expired = True
	except BadSignature:
		invalid = True

	if data:
		user = faycare.models.user_datastore.find_user(id=data[0])

	expired = expired and (user is not None)
	return expired, invalid, user

def _commit(response=None):
	faycare.models.user_datastore.commit()
	return response

def update_password(user, password):
	from flask.ext.security.utils import encrypt_password
	from flask.ext.security.signals import password_reset
	import mandrill

	user.password = encrypt_password(password)
	faycare.models.user_datastore.put(user)
	template = render_template('front/email/reset_password.html')
	message_obj = {
		'html':template,
		'subject':'[GoTotGo] Password Reset Notification',
		'from_email':'jacob@gototgo.com',
		'to':[{
				'email':user.email,
				'name': user.person.first_name
			}],
		'view_content_link':False,
		'tags':['password reset']
	}
	m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)
	m.messages.send(message_obj)
	password_reset.send(app._get_current_object(), user=user)

class ResetPasswordView(MethodView):
	def get(self,token=None):
		if not token:
			return redirect(url_for('front.forgot_password'))
		expired, invalid, user = get_token_status(token, 'reset', 'RESET_PASSWORD')
		
		if invalid:
			flash('That is not a valid password reset link')
		if expired:
			flash('Your password reset link has expired.')
		if invalid or expired:
			return redirect(url_for('front.forgot_password'))

		form = faycare.models.security.reset_password_form()
		return render_template('front/reset_password.html',
			reset_password_form=form,
			token=token)

	def post(self,token):
		from flask.ext.security.recoverable import update_password
		from flask.ext.security.utils import login_user
		expired, invalid, user = get_token_status(token, 'reset', 'RESET_PASSWORD')

		if invalid:
			flash('That is not a valid password reset link')
		if expired:
			flash('Your password reset link has expired.')
		if invalid or expired:
			return redirect(url_for('front.forgot_password'))

		form = faycare.models.security.reset_password_form()

		if form.validate_on_submit():
			after_this_request(_commit)
			update_password(user, form.password.data)
			flash('Your password has been reset.')
			login_user(user)
			return redirect('/')

		return render_template('front/reset_password.html',
			reset_password_form=form,
			token=token)

class ChooseSchoolForm(Form):
	school_choice = RadioField()

def unique_email(form, field):
	print field.data
	print Invite.query.filter(Invite.email==field.data).first()
	if Invite.query.filter(Invite.email==field.data).first() is not None:
		msg = 'A request with this email was already submitted'
		raise ValidationError(msg)

class InviteForm(Form):
	email = TextField('Enter your email address',validators=[validators.Required(),validators.Email(),unique_email])
	school = TextField('School name, city, and state', validators=[validators.Required()])

class SetupSchoolForm(Form):
	name = TextField('Name of your school',validators=[validators.Required()])
	city = TextField(validators=[validators.Required()])
	state = TextField('State abbv',validators=[validators.Required()])
	timezone = HiddenField()
	new_existing = RadioField(choices=[('new','New'),('existing','Existing')])

# front.add_url_rule('/',view_func=InviteView.as_view('invite'))
front.add_url_rule('/',view_func=HomeView.as_view('home'))
front.add_url_rule('/setup/', view_func=SetupRoleView.as_view('setup'))
front.add_url_rule('/setup/1/', view_func=SetupRoleView.as_view('setup1'))
front.add_url_rule('/setup/2/', view_func=SetupSchoolView.as_view('setup2'))
front.add_url_rule('/connect/', view_func=StripeConnectView.as_view('stripe_connect'))
front.add_url_rule('/tour/', view_func=TourView.as_view('tour'))
front.add_url_rule('/forgot/', view_func=ForgotPasswordView.as_view('forgot_password'))
front.add_url_rule('/reset_password/<token>/', view_func=ResetPasswordView.as_view('reset_password'))