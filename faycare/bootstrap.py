from flask.ext.script import Command
from flask.ext.security.utils import encrypt_password
import faycare.models

def create_plans():
	plans = [('free',1,0,0),('small',40,100,2),('medium',70,150,1.5),('large',100,188,1),('unlimited',400,480,0.9)]
	for p in plans:
		plan = faycare.models.Plan(name=p[0],limit=p[1],price=p[2],overage=p[3])
		faycare.models.db.session.add(plan)
	faycare.models.db.session.commit()

def create_order_priorities():
	priorities = ['Dead in the water','Show stopper','High','Low','Whenever']
	for p in priorities:
		priority = faycare.models.OrderPriority(name=p)
		faycare.models.db.session.add(priority)
	faycare.models.db.session.commit()	

def create_order_types():
	order_types = ['Purchase Order','Maintenance Order']
	for o in order_types:
		order_type = faycare.models.OrderType(name=o)
		faycare.models.db.session.add(order_type)
	faycare.models.db.session.commit()	

def create_roles():
	for role in (('main_admin',True), ('director',True), ('admin',False),('teacher',False),('parent',False),('peasent',True),('attendance',True)):
		faycare.models.user_datastore.create_role(name=role[0], 
			description=role[0],
			private=role[1])
	faycare.models.user_datastore.commit()

def create_activity_types():
	activities = [('Circle Time','rocket'),('Playground','square'),('Breakfast','circle-o'),('Lunch','heart'),('Bathroom','cutlery'),('Snack','leaf'),('Nap','moon-o'),('Playtime','bell'),('Other','bell')]
	for activity in activities:
		new_activity_type = faycare.models.ActivityType(name=activity[0],icon=activity[1])
		faycare.models.db.session.add(new_activity_type)
	faycare.models.db.session.commit()

def create_credit_types():
	credit_types = ['government','scholarship','prorate','other','balance']
	for credit in credit_types:
		new_credit_type = faycare.models.CreditType(name=credit)
		faycare.models.db.session.add(new_credit_type)
	faycare.models.db.session.commit()

class PopulateDB(Command):
	"""Fills in predefined data into DB"""
	def run(self, **kwargs):
		create_plans()
		create_roles()
		create_activity_types()
		create_credit_types()
		create_order_priorities()
		create_order_types()

class ResetDB(Command):
	"""Drops all tables and recreates them"""
	def run(self, **kwargs):
		faycare.models.db.session.commit()
		faycare.models.db.drop_all()
		faycare.models.db.create_all()

class EraseDB(Command):
	"""Drops all tables period"""
	def run(self, **kwargs):
		faycare.models.db.drop_all()

print 'bootstrap'