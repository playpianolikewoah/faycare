$(function(){
	$("a.delete-parent").on('click',function(e){
		e.preventDefault()
		var $this = $(this)
		if(confirm('are you sure you want to delete this parent?')){
			var parentID = $this.attr('data-id')
			$this.closest('tr').remove();	
			$.ajax({
				url: '/' + school + '/parents/' + parentID + '/',
				type: 'DELETE',
				success: function(data){
					if($this.closest('table').find('tr').length == 0){
						location.reload();
					}
				}
			})

		}
		
	});	

	$("a.send-message-parent").on('click',function(e){
		e.preventDefault();
		$("#message_parent").val($(this).attr('data-id'));
		$("#message-who").empty().append('<option value="parent"></option>')
		$("#message-to").text(' ' + $(this).attr('data-name'))
		$("#message-center").trigger('open');
	});
});