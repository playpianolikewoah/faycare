$(function(){
	initFastButtons();
	var $attendanceAlertMessge = $("#attendance-alert-message");
	var $attendanceWarningMessge = $("#attendance-warning-message");
	$("#code").prop('disabled',true);
	//Hide toggle nav
	$("a.uk-navbar-toggle").hide();
	$("#right-nav").hide();
	$("a.dial-button").on("click",function(e){
		e.preventDefault();
		$attendanceAlertMessge.text('');
		$attendanceWarningMessge.text('');
		var $code = $("#code");
		if($(this).hasClass('uk-button-success')){
			checkCode($code.val())
			$("#code").val('');
		}else if($(this).hasClass('uk-button-danger')){
			$code.val($code.val().substring(0, $code.val().length - 1));
		}else{
			$code.val($code.val() + $(this).text())	
		}
	})

	var modal = new $.UIkit.modal.Modal("#kid-chooser-modal");
	var kidCount = 0;
	var checkinCount = 0;
	var currentUser = 0;
	var personType = 1;
	var status = 1;
	var currentStatus = 'in';
	var checkCode = function(code){
		$.post(postURL,{code:code},function(data){
			if(data.status == 'OK'){
				currentUser = data.id;
				if(data.role == 'parent'){
					$("#kid-choices").empty();
					var s = '';
					kidCount = data.kids.length;
					checkinCount = 0;
					$("#kid-choices").append('<h3>Hey there ' + data.name + '</h3>');
					if(!data.kids.length){
						$("#kid-choices").append('<p class="uk-text-danger">No kids available.</p>');
					}
					for(s in data.kids){
						console.log(data.kids[s]);
						if(data.kids[s].hold){
							$("#kid-choices").append('<button type="button" data-id="' + data.kids[s].id + '" class="kid-button uk-button uk-width-1-1" disabled>' + data.kids[s].name + '(hold)</button>');
						}else{
							$("#kid-choices").append('<a href="#" data-id="' + data.kids[s].id + '" class="kid-button uk-button uk-button-primary uk-width-1-1">' + data.kids[s].name + '</a>');	
						}
						
					}
					$("#kid-choices").append('<a href="#" class="done-kids uk-button uk-width-1-1" >Done</a>')
					modal.show();
				}else if(data.role == 'staff'){
					$("#kid-choices").empty();
					for(s in data.kids){
						$("#kid-choices").append('<a href="#" data-id="' + data.kids[s].id + '" class="kid-button uk-button uk-button-primary uk-width-1-1">' + data.kids[s].name + '</a>');
					}
					$("#kid-choices").append('<h3>Hey there ' + data.name + '</h3>');
					$("#kid-choices").append('<a href="#" class="done-staff uk-button uk-width-1-1" >Sign me ' + currentStatus + '</a>')
					modal.show();	
				}
				

			}else if(data.status == 'EMPTY'){
				$("#code").val('');
				$("body").stop().css("background-color", "#FF0000")
				.animate({ "background-color": "#FFFFFF"}, 1500);
				$("#failure-display").slideDown(300).delay(1000).slideUp(300);
			}
		});
	}

	$('body').on('click','a.done-kids',function(e){
		e.preventDefault();
		if(modal.isActive()){
			modal.hide();
		}
	});

	$('body').on('click','a.done-staff',function(e){
		e.preventDefault();
		if(modal.isActive()){
			modal.hide();
		}
		$.post(postURL,{staff:currentUser,
			status:status
		},function(data){
			$("body").stop().css("background-color", "#FFFF9C")
				.animate({ "background-color": "#FFFFFF"}, 1500);
		});
	});

	$("#sign-in").on("click",function(e){
		e.preventDefault();
		status = 1;
		currentStatus = 'in';
	});

	$("#sign-out").on("click",function(e){
		e.preventDefault();
		status = 0;
		currentStatus = 'out';
	});

	$("#parent").on("click",function(e){
		e.preventDefault();
		personType = 1;
	});

	$("#staff").on("click",function(e){
		e.preventDefault();
		personType = 0;
	});

	var lostConnection = false;

	window.setInterval(pingServer,10000)

	function pingServer(){
		$.post(pingURL,function(data){
			if(data.status == 'OK'){
				if(lostConnection){
					$attendanceAlertMessge.text('And we\'re back.');
					lostConnection = false;
				}else{
					$attendanceAlertMessge.text('');	
				}
			}else if(data.status == 'NOGOOD'){
				$attendanceAlertMessge.text('No longer logged in. Sending you to login.');
				setTimeout(sendToLogin,2000)
				function sendToLogin(){
					window.location.href = '/login/'	
				}
			}
		})
		.fail(function(){
			$attendanceAlertMessge.text('Internet connection lost. Rechecking...')
			lostConnection = true;
		});
	}



	$('body').on('click','a.kid-button',function(e){
		e.preventDefault();
		if($(this).attr('disabled')){
			return;
		}
		$(this)
		.removeClass('uk-button-primary')
		.addClass('uk-button-success')
		.attr('disabled','disabled')
		checkinCount++;
		if(modal.isActive() && checkinCount == kidCount){
			modal.hide();
		}
		$.post(postURL,{kid:$(this).attr('data-id'),
			parent:currentUser,
			status:status
		},function(data){
			
			if(data.status == 'OK'){
				$("#success-display").slideDown(300).delay(1000).slideUp(300);
			}else{
				$("#failure-display").slideDown(300).delay(1000).slideUp(300);
				if(data.status == 'NOGOOD'){
					$("#attendance-warning-message").text(data.message);
				}
			}
			$("body").stop().css("background-color", "#FFFF9C")
				.animate({ "background-color": "#FFFFFF"}, 1500);
		});
	});
});