$(function(){
	var edittedPrice = 0;
	var priceTemplate = '';
	var $modal = new $.UIkit.modal.Modal("#edit-price-modal");
	var $row = '';
	$("a.edit-price").on('click',function(e){
		$row = $(this).closest('tr');
		edittedPrice = $(this).attr('data-id');
		var priceTemplate = '/' + schoolID + '/prices/edit/' + edittedPrice + '/';
		$.get(priceTemplate,function(data){
			$("#price-edit-form").html(data);
			$modal.show()
		});
	});

	$("body").on("submit","#price-edit-form form",function(e){
		e.preventDefault();
		var data = {
			'price' : $("#price").val(),
			'billing_duration': $("#billing_duration").val(),
			'billing_unit': $("#billing_unit").val(),
			'late_price' : $("#late_price").val()
		};

		$.post('/' + schoolID + '/prices/edit/' + edittedPrice + '/',data,function(data){
			var units = ['days','months','years','hours','minutes','seconds'];
			$modal.hide();
			$row.find('td.billing-duration span.duration').text(data.billing_duration);
			$row.find('td.billing-duration span.unit').text(units[parseInt(data.billing_unit)]);
			$row.find('td.price span.price').text('$' + data.price.toFixed(2));
			$row.find('td.price span.late-price').text('($' + data.late_price.toFixed(2) + ' late fee)');
		});
		return false;
	});
});