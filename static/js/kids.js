$(function(){
	$("a.delete-kid").on('click',function(e){
		e.preventDefault()
		if(confirm('are you sure you want to delete this kid?')){
			var kidID = $(this).attr('data-id');
			$(this).closest('tr').remove();	
			$.ajax({
				url: '/' + school + '/kids/' + kidID + '/',
				type: 'DELETE',
				success: function(data){
					//
				}
			})

		}
		
	});

	var more = 1;
	$('.table-detailer').waypoint('infinite',{
		container: '#infinite-table',
		onAfterPageLoad: function(e){
			console.log('on after page load?',e)
		}
	});
	var modal = new $.UIkit.modal.Modal("#message-modal");
	$("a.message-button").on('click',function(e){
		e.preventDefault();
		$("#message_kid").val($(this).attr('data-id'));
		$("#message-who").empty().append('<option value="kid"></option>');
		$("#message-to").text('the parents of ' + $(this).attr('data-name'))
		$("#message-center").trigger('open');
	});

	var alertWarning = function(message,data){
		if(data == 'OKAY'){
			$("#drop-primary")
			.html(message)
	    	.removeClass('uk-hidden')
	    	.delay(5000)
	    	.slideUp();
		}else{
			$("#drop-modal-primary")
			.html(message)
	    	.removeClass('uk-hidden')
	    	.delay(5000)
	    	.slideUp();
		}
	}
});